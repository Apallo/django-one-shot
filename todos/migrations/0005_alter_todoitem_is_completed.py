# Generated by Django 4.1.1 on 2022-09-07 20:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0004_alter_todoitem_is_completed"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="is_completed",
            field=models.BooleanField(null=True),
        ),
    ]
