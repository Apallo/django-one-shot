from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.


class TodoListadmin(admin.ModelAdmin):
    pass


class TodoItemadmin(admin.ModelAdmin):
    pass


admin.site.register(TodoList, TodoListadmin)
admin.site.register(TodoItem, TodoItemadmin)
